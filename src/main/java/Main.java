import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    
    private static final String CONNECTION_URL = "jdbc:sqlserver://localhost\\SQLEXPRESS;loginTimeout=30;";
    private static final String USERNAME = "sa";
    private static final String PASSWORD = "Pa$$w0rd";
    
    private static final String DATABASE_NAME = "EjemploJdbc";
    
    private static final String TABLE_ONE_NAME = "Alumno";
    private static final String SQL_CREATE_TABLE_ONE = "CREATE TABLE " + TABLE_ONE_NAME + " ("
            + "Id INT NOT NULL,"
            + "Nombre VARCHAR(45) NOT NULL,"
            + "Apellido VARCHAR(45) NOT NULL,"
            + "PRIMARY KEY(Id))";
    
    private static final String TABLE_TWO_NAME = "Clase";
    private static final String SQL_CREATE_TABLE_TWO = "CREATE TABLE " + TABLE_TWO_NAME + " ("
            + "Id INT NOT NULL,"
            + "Nombre VARCHAR(45) NOT NULL,"
            + "Descripcion VARCHAR(MAX) NOT NULL,"
            + "PRIMARY KEY(Id))";
    
    private static final String TABLE_JOIN_NAME = "Matricula";
    private static final String SQL_CREATE_TABLE_JOIN = "CREATE TABLE " + TABLE_JOIN_NAME + " ("
            + "Id INT NOT NULL,"
            + "IdAlumno INT NOT NULL,"
            + "IdClase INT NOT NULL,"
            + "Fecha DATE NOT NULL,"
            + "PRIMARY KEY(Id),"
            + "FOREIGN KEY (IdAlumno) REFERENCES Alumno(Id),"
            + "FOREIGN KEY (IdClase) REFERENCES Clase(Id))";
    
    public static void main (String[] args){
        System.out.println("Ejemplo de acceso a SQL Server con JDBC");
        Connection connection = null;
        
        try {
            connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
            var connectionMetadata = connection.getMetaData();
            logMessage("Conectado a SQL Server con driver " + connectionMetadata.getDriverName());
            
            var statement = connection.createStatement();
            
            var sqlDropDatabase = "DROP DATABASE IF EXISTS " + DATABASE_NAME + ";";
            statement.execute(sqlDropDatabase);
            var sqlCreateDatabase = "CREATE DATABASE " + DATABASE_NAME + ";";
            statement.execute(sqlCreateDatabase);
            logMessage("Base de datos re-creada");
            
            statement.execute("USE " +  DATABASE_NAME  + ";");
            logMessage("Usando contexto de base de datos " + DATABASE_NAME);
            
            statement.execute(SQL_CREATE_TABLE_ONE);
            logMessage("Tabla " + TABLE_ONE_NAME + " creada");
            
            statement.execute(SQL_CREATE_TABLE_TWO);
            logMessage("Tabla " + TABLE_TWO_NAME + " creada");
            
            statement.execute(SQL_CREATE_TABLE_JOIN);
            logMessage("Tabla " + TABLE_JOIN_NAME + " creada");
            
            // Ejercicio 1
            // Insertar alumnos Ted Codd, Martin Fowler, Greg Young y Udi Dahan
            statement.execute("INSERT INTO Alumno([Id],[Nombre],[Apellido]) VALUES"
                    + "(1, 'Ted', 'Codd'),"
                    + "(2, 'Martin', 'Fowler'),"
                    + "(3, 'Greg', 'Young'),"
                    + "(4, 'Udi', 'Dahan');");
            
            // Ejercicio 2
            // Insertar clases Sistemas Distribuidos, Bases de Datos, CQRS y Technical English
            statement.execute("INSERT INTO Clase([Id],[Nombre],[Descripcion]) VALUES"
                    + "(1, 'Sistemas Distribuidos', 'Escalabilidad horizontal, mensajería y EDA'),"
                    + "(2, 'Bases de Datos', 'Cómo persistir y recuperar datos'),"
                    + "(3, 'CQRS', 'Conocimientos de Command Query Responsibility Segregation'),"
                    + "(4, 'Technical English', 'inglés técnico');");
            
            // Ejercicio 3
            // Matricular a Ted Codd en Bases de datos en cualquier fecha de 1971
            // Matricular a Martin Fowler en Sistemas Distribuidos y Bases de datos en cualquier fecha de 2000
            // Matricular a Greg Young en Sistemas Distribuidos, Bases de datos y CQRS en cualquier fecha de 2003
            // Matricular a Udi Dahan en Sistemas Distribuidos, Bases de datos y CQRS en cualquier fecha de 2010
            statement.execute("INSERT INTO Matricula([Id],[IdAlumno],[IdClase],[Fecha]) VALUES"
                    + "(1, 1, 2, '1971-1-1'),"
                    + "(2, 2, 1, '2000-1-1'),"
                    + "(3, 2, 2, '2000-1-1'),"
                    + "(4, 3, 1, '2003-1-1'),"
                    + "(5, 3, 2, '2003-1-1'),"
                    + "(6, 3, 3, '2003-1-1'),"
                    + "(7, 4, 1, '2010-1-1'),"
                    + "(8, 4, 2, '2010-1-1'),"
                    + "(9, 4, 3, '2010-1-1');");
            
            // Ejercicio 4
            // Mostrar en consola todos los alumnos
            // Mostrar en consola todas las clases
            var alumnosResultSet = statement.executeQuery("SELECT Nombre, Apellido FROM Alumno");
            logMessage("ALUMNOS:");
            while(alumnosResultSet.next()){
                var nombre = alumnosResultSet.getString("Nombre");
                var apellido = alumnosResultSet.getString("Apellido");
                logMessage(nombre + " " + apellido);
            }
            
            var clasesResultSet = statement.executeQuery("SELECT Nombre FROM Clase");
            logMessage("CLASES:");
            while(clasesResultSet.next()){
                var nombre = clasesResultSet.getString("Nombre");
                logMessage(nombre);
            }
            
            // Ejercicio 5
            // Mostrar en consola todas las clases y el número de alumnos que se han matriculado
            var matriculacionesResultSet = statement.executeQuery("SELECT c.Nombre, COUNT(m.Id) as Total "
                    + "FROM Clase c LEFT JOIN Matricula m ON c.Id = m.IdClase "
                    + "GROUP BY c.Nombre;");
            logMessage("MATRICULACIONES:");
            while(matriculacionesResultSet.next()){
                var nombre = matriculacionesResultSet.getString("Nombre");
                var total = matriculacionesResultSet.getString("Total");
                logMessage(nombre + " " + total);
            }
            
            // Ejercicio 6
            // Mostrar en consola todas las clases y el número de alumnos que se han matriculado antes de 2000
            var matriculacionesAntiguasResultSet = statement.executeQuery("SELECT c.Nombre, COUNT(m.Id) as Total "
                    + "FROM Clase c LEFT JOIN Matricula m ON c.Id = m.IdClase "
                    + "WHERE m.Fecha < '2000-1-1' "
                    + "GROUP BY c.Nombre;");
            logMessage("MATRICULACIONES ANTES DE 2000:");
            while(matriculacionesAntiguasResultSet.next()){
                var nombre = matriculacionesAntiguasResultSet.getString("Nombre");
                var total = matriculacionesAntiguasResultSet.getString("Total");
                logMessage(nombre + " " + total);
            }
            
            // Ejercicio 7
            // Mostrar en consola todos los alumnos y el número de clases en el que se han matriculado
            var matriculacionesDetalladasResultSet = statement.executeQuery("SELECT a.Nombre, a.Apellido, COUNT(m.Id) as Total "
                    + "FROM Alumno a LEFT JOIN Matricula m ON a.Id = m.IdAlumno "
                    + "GROUP BY a.Nombre, a.Apellido;");
            logMessage("MATRICULACIONES DE ALUMNOS:");
            while(matriculacionesDetalladasResultSet.next()){
                var nombre = matriculacionesDetalladasResultSet.getString("Nombre");
                var apellido = matriculacionesDetalladasResultSet.getString("Apellido");
                var total = matriculacionesDetalladasResultSet.getString("Total");
                logMessage(nombre + " " + apellido + " " + total);
            }
        } catch (SQLException exception) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, exception);
        } finally {
            closeConnection(connection);
        }
    }
    
    private static void closeConnection(Connection connection) {
        if(connection != null){
            try {
                connection.close();
                logMessage("Conexión con SQL Server cerrada");
            } catch (SQLException exception) {
                logException(exception);
            }
        }
    }
    
    private static void logException(Exception exception){
        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, exception);
    }
    
    private static void logMessage(String message){
        System.out.println(message);
    }
}
